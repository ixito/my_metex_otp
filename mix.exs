defmodule MyMetexOtp.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_metex_otp,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [
        plt_add_deps: :apps_direct
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:json, "~> 1.3"},
      {:ex_doc, "~> 0.22"},
      {:httpoison, "~> 1.6"},
      {:mox, "~> 0.5"},
      {:doctor, "~> 0.13"},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end
end
