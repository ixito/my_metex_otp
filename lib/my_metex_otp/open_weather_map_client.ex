defmodule MyMetexOtp.OpenWeatherMapClient do
  alias MyMetexOtp.OpenWeatherMapBehaviour

  @behaviour OpenWeatherMapBehaviour

  @api_key Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)

  @moduledoc """
  OpenWeatherMap web service client adapter. Performs API calls via libraries HTTPoison & JSON.
  """

  @doc """
  Construct the URL that is used to call the OpenWeatherMap API.

  ## Example
      iex> MyMetexOtp.OpenWeatherMapClient.url_for "Seattle"
      "http://api.openweathermap.org/data/2.5/weather?q=Seattle&APPID=d834c01584439f68398872a506d84693"
  """
  @spec url_for(String.t()) :: String.t()
  def url_for(location) when is_binary(location),
    do: "http://api.openweathermap.org/data/2.5/weather?q=#{location}&APPID=#{apikey()}"

  @doc """
  Provide the OpenWeatherMap API key

  ## Example
      iex> MyMetexOtp.OpenWeatherMapClient.apikey
      "d834c01584439f68398872a506d84693"
  """
  @spec apikey() :: String.t()
  def apikey, do: @api_key

  @doc """
  Perform an HTTPoison.get() against the OpenWeatherMapClient url

  ## Example
      iex> alias MyMetexOtp.OpenWeatherMapClient, as: ClientApi
      iex> (
      iex>  "Seattle"
      iex>  |> ClientApi.url_for
      iex>  |> ClientApi.get()
      iex> )
      {:ok, %HTTPoison.Response{}}
  """
  @spec get(String.t()) :: {:ok, %HTTPoison.Response{}} | {:error, %HTTPoison.Error{}}
  @impl OpenWeatherMapBehaviour
  def get(url) when is_binary(url), do: HTTPoison.get(url)

  # -----------------------------------------------------------------------------------------------------------------------
  # Example HTTP request to OpenWeatherMap for Seattle: http://api.openweathermap.org/data/2.5/weather?q=Seattle&APPID=d834c01584439f68398872a506d84693
  # Example JSON response:
  # {"coord":{"lon":-122.33,"lat":47.61},
  #  "weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],
  #  "base":"cmc stations","main":{"temp":282.93,"pressure":1018,"humidity":87,"temp_min":280.15,"temp_max":284.45},
  #  "wind":{"speed":2.1,"deg":350},"clouds":{"all":75},"dt":1460041234,
  #  "sys":{"type":1,"id":2949,"message":0.0036,"country":"US","sunrise":1460036013,"sunset":1460083767},
  #  "id":5809844,"name":"Seattle","cod":200}
  # -----------------------------------------------------------------------------------------------------------------------

  # -----------------------------------------------------------------------------------------------------------------------
  # Example HTTPoison request to OpenWeatherMap for Seattle data:
  # iex> HTTPoison.get "http://api.openweathermap.org/data/2.5/weather?q=Seattle&APPID=d834c01584439f68398872a506d84693"
  #
  # Example JSON response:
  # {:ok,
  #  %HTTPoison.Response{body: "{\"coord\":{\"lon\":-122.33,\"lat\":47.61},\"weather\":[{\"id\":803,\"main\":\"Clouds\",
  #  \"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"cmc stations\",\"main\":{\"temp\":283.1,
  #  \"pressure\":1018,\"humidity\":87,\"temp_min\":280.15,\"temp_max\":284.75},\"wind\":{\"speed\":2.1,\"deg\":350},
  #  \"clouds\":{\"all\":75},\"dt\":1460041843,\"sys\":{\"type\":1,\"id\":2949,\"message\":0.0052,\"country\":\"US\",
  #  \"sunrise\":1460036012,\"sunset\":1460083768},\"id\":5809844,\"name\":\"Seattle\",\"cod\":200}",
  #  headers: [{"Server", "openresty"}, {"Date", "Thu, 07 Apr 2016 15:19:27 GMT"},
  #   {"Content-Type", "application/json; charset=utf-8"},
  #   {"Content-Length", "436"}, {"Connection", "keep-alive"},
  #   {"X-Cache-Key",
  #    "/data/2.5/weather?APPID=d834c01584439f68398872a506d84693&q=seattle"},
  #   {"Access-Control-Allow-Origin", "*"},
  #   {"Access-Control-Allow-Origin", "*"},
  #   {"Access-Control-Allow-Methods", "GET, POST"}], status_code: 200}}
  # -----------------------------------------------------------------------------------------------------------------------

  # -----------------------------------------------------------------------------------------------------------------------
  # Example JSON.decode! of a JSON body:
  # iex> JSON.decode! "{\"coord\":{\"lon\":-122.33,\"lat\":47.61},\"weather\":[{\"id\":500,\"main\":\"Rain\",
  # ...> \"description\":\"light rain\",\"icon\":\"10d\"}],\"base\":\"cmc stations\",\"main\":{\"temp\":291.28,
  # ...> \"pressure\":1014,\"humidity\":68,\"temp_min\":289.26,\"temp_max\":293.15},\"wind\":{\"speed\":4.6,\"deg\":180,
  # ...> \"gust\":8.2},\"rain\":{\"1h\":0.51},\"clouds\":{\"all\":90},\"dt\":1467920837,\"sys\":{\"type\":1,\"id\":2949,
  # ...>\"message\":0.0059,\"country\":\"US\",\"sunrise\":1467894062,\"sunset\":1467950851},\"id\":5809844,\"name\":\"Seattle\",\"cod\":200}"
  #
  # Example JSON response:
  # %{"base" => "cmc stations", "clouds" => %{"all" => 90}, "cod" => 200,
  #   "coord" => %{"lat" => 47.61, "lon" => -122.33}, "dt" => 1467920837,
  #   "id" => 5809844,
  #   "main" => %{"humidity" => 68, "pressure" => 1014, "temp" => 291.28,
  #     "temp_max" => 293.15, "temp_min" => 289.26}, "name" => "Seattle",
  #   "rain" => %{"1h" => 0.51},
  #   "sys" => %{"country" => "US", "id" => 2949, "message" => 0.0059,
  #     "sunrise" => 1467894062, "sunset" => 1467950851, "type" => 1},
  #   "weather" => [%{"description" => "light rain", "icon" => "10d", "id" => 500,
  #      "main" => "Rain"}],
  #   "wind" => %{"deg" => 180, "gust" => 8.2, "speed" => 4.6}}
  # -----------------------------------------------------------------------------------------------------------------------

  @doc ~S"""
  Parse an HTTP response for its JSON body

  ## Examples
      iex> MyMetexOtp.OpenWeatherMapClient.parse_response({:ok,
      ...> %HTTPoison.Response{body: "{\"coord\":{\"lon\":-122.33,\"lat\":47.61},\"weather\":[{\"id\":804,
      ...> \"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"base\":\"cmc stations\",
      ...> \"main\":{\"temp\":287.39,\"pressure\":1020,\"humidity\":82,\"temp_min\":285.15,\"temp_max\":289.15},
      ...> \"wind\":{\"speed\":3.1,\"deg\":130},\"clouds\":{\"all\":90},\"dt\":1467742665,\"sys\":{\"type\":1,\"id\":2949,
      ...> \"message\":0.0038,\"country\":\"US\",\"sunrise\":1467721161,\"sunset\":1467778115},\"id\":5809844,
      ...> \"name\":\"Seattle\",\"cod\":200}",
      ...> headers: [{"Server", "openresty"}, {"Date", "Tue, 05 Jul 2016 18:25:55 GMT"},
      ...>  {"Content-Type", "application/json; charset=utf-8"},
      ...>  {"Content-Length", "439"}, {"Connection", "keep-alive"},
      ...>  {"X-Cache-Key",
      ...>   "/data/2.5/weather?APPID=d834c01584439f68398872a506d84693&q=seattle"},
      ...>  {"Access-Control-Allow-Origin", "*"},
      ...>  {"Access-Control-Allow-Credentials", "true"},
      ...>  {"Access-Control-Allow-Methods", "GET, POST"}], status_code: 200}})
      {:ok, 14.2}

      iex> MyMetexOtp.OpenWeatherMapClient.parse_response {:error, %HTTPoison.Error{id: nil, reason: :nxdomain}}
      :error
  """
  @spec parse_response({:ok, %HTTPoison.Response{}}) :: {:ok, number()} | :error
  def parse_response({:ok, %HTTPoison.Response{body: body, status_code: 200}}) do
    body
    |> JSON.decode!()
    |> compute_temperature
  end

  def parse_response(_), do: :error

  @doc ~S"""
  Compute from a json payload a temperature in Celsius from a provided temperature in Kelvin

  ## Examples
      iex> MyMetexOtp.OpenWeatherMapClient.compute_temperature(%{"base" => "cmc stations", "clouds" => %{"all" => 75}, "cod" => 200,
      ...> "coord" => %{"lat" => 47.61, "lon" => -122.33},"dt" => 1460041843, "id" => 5809844, "main" => %{"humidity" => 87,
      ...> "pressure" => 1018, "temp" => 283.1,"temp_max" => 284.75, "temp_min" => 280.15}, "name" => "Seattle",
      ...> "sys" => %{"country" => "US", "id" => 2949,"message" => 0.0052, "sunrise" => 1460036012, "sunset" => 1460083768,
      ...> "type" => 1},"weather" => [%{"description" => "broken clouds", "icon" => "04d", "id" => 803, "main" => "Clouds"}],
      ...> "wind" => %{"deg" => 350, "speed" => 2.1}})
      {:ok, 10.0}

      iex> MyMetexOtp.OpenWeatherMapClient.compute_temperature %{"bad" => "json"}
      :error
  """
  @spec compute_temperature(%{String.t() => String.t()}) :: {:ok, number()} | :error
  def compute_temperature(json) when is_map(json) do
    try do
      # api.openweathermap returns results in Kelvin; round to 1 decimal place
      temp =
        (json["main"]["temp"] - 273.15)
        |> Float.round(1)

      {:ok, temp}
    rescue
      _fail -> :error
    end
  end
end
