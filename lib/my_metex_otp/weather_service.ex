defmodule MyMetexOtp.WeatherService do
  alias MyMetexOtp.OpenWeatherMapClient, as: ClientApi

  @moduledoc """
  A service to provide weather temperatures for a location.

  Involves making an HTTP request to an external weather service, and parsing the JSON response to extract the
  temperature.
  """

  @doc """
  Accepts a single argument containing a location, and reports the temperature in Celsius.

  ## Examples
      iex> MyMetexOtp.WeatherService.temperature_of "Seattle"
      "Seattle: 14.7°C"

      iex> cities = ["Singapore", "Monaco", "Seattle", "Amsterdam", "Macau"]
      iex> cities |> Enum.map(fn city -> MyMetexOtp.WeatherService.temperature_of(city) end)
      ["Singapore: 28.9°C", "Monaco: 24.7°C", "Seattle: 15.8°C", "Amsterdam: 17.7°C", "Macau: 29.6°C"]

      iex> cities |> Enum.map( &(MyMetexOtp.WeatherService.temperature_of(&1)) )
      ["Singapore: 28.9°C", "Monaco: 24.7°C", "Seattle: 15.8°C", "Amsterdam: 17.7°C", "Macau: 29.6°C"]
  """
  @spec temperature_of(String.t()) :: {:ok, number()} | :error
  def temperature_of(location) when is_binary(location) do
    location
    |> ClientApi.url_for()
    |> ClientApi.get()
    |> ClientApi.parse_response()
  end

  @doc ~S"""
  Update server stats based on location.

  If location exists in stats, the location stats is incremented, otherwise the location is added.

  ## Example
      iex> stats = MetexOtp.WorkerHelper.update_stats(%{}, "Seattle")
      %{"Seattle" => 1}
      iex> stats = MetexOtp.WorkerHelper.update_stats(stats, "Seattle")
      %{"Seattle" => 2}
  """
  @spec update_stats(%{}, String.t()) :: %{String.t() => non_neg_integer()}
  def update_stats(old_stats, location) when is_map(old_stats) and is_binary(location) do
    case Map.has_key?(old_stats, location) do
      true ->
        # &(&1 + 1) == fn(val) -> val + 1 end
        Map.update!(old_stats, location, &(&1 + 1))

      false ->
        Map.put_new(old_stats, location, 1)
    end
  end
end
