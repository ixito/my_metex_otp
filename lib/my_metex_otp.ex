defmodule MyMetexOtp do
  @moduledoc """
  A service that reports the current temperature of a given city and the frequency of requested locations.
  """

  alias MyMetexOtp.Server

  @doc ~S"""
  Start the temperature service.

  ## Example
      iex> {:ok, pid} = MyMetexOtp.start_service
  """
  @spec start_service() :: {:ok, pid()}
  defdelegate start_service, to: Server

  @doc ~S"""
  Get the temperature of a location.

  ## Example
      iex> {:ok, pid} = MyMetexOtp.start_service
      iex> MyMetexOtp.get_temperature("Singapore")
      "25.6°C"

      iex> cities = ["Singapore", "Monaco", "Seattle", "Amsterdam", "Macau"]
      iex> cities |> Enum.map(fn city -> MyMetexOtp.get_temperature(city) end)
      ["26.2°C", "20.1°C", "7.6°C", "18.4°C", "23.0°C"]

      iex> cities = ["Singapore", "Monaco", "Seattle", "Amsterdam", "Macau"]
      iex> cities |> Enum.map( &(MyMetexOtp.get_temperature(&1)) )
      ["26.2°C", "20.1°C", "7.6°C", "18.4°C", "23.0°C"]
  """
  @spec get_temperature(String.t()) :: String.t()
  defdelegate get_temperature(location), to: Server

  @doc ~S"""
  Get the current stats regarding number of calls made for city temperatures.

  ## Example
      iex> {:ok, pid} = MyMetexOtp.start_service
      iex> MyMetexOtp.get_temperature("Singapore")
      iex> MyMetexOtp.get_temperature("Seattle")
      iex> MyMetexOtp.get_stats
      %{"Seattle" => 1, "Singapore" => 1}
  """
  @spec get_stats() :: %{String.t() => non_neg_integer()}
  defdelegate get_stats, to: Server

  @doc ~S"""
  Re-initialize server stats.

  ## Example
      iex> {:ok, pid} = MyMetexOtp.start_service
      iex> MyMetexOtp.get_temperature "Seattle"
      iex> MyMetexOtp.get_stats
      %{"Seattle" => 1}
      iex> MyMetexOtp.reset_stats
      :ok
      iex> MyMetexOtp.get_stats
      %{}
  """
  @spec reset_stats() :: :ok
  defdelegate reset_stats, to: Server

  @doc ~S"""
  Stop the temperature service.

  ## Example
      iex> {:ok, pid} = MyMetexOtp.start_service
      iex> Process.alive? pid
      true
      iex> MyMetexOtp.stop_service
      :ok
      iex> Process.alive? pid
      false
  """
  @spec stop_service() :: :ok
  defdelegate stop_service, to: Server
end
