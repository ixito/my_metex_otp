import Config

config :my_metex_otp,
  open_weather_map_adapter: MyMetexOtp.OpenWeatherMapClient

# example use: "value1" = Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)
# example use: @api_key Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)
