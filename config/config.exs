import Config

config :my_metex_otp,
  open_weather_map_api_key: "d834c01584439f68398872a506d84693",
  open_weather_map_adapter: MyMetexOtp.OpenWeatherMapClient

# expects to find both dev & test.exs files
import_config "#{Mix.env()}.exs"

# example use: "value1" = Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)
# example use: @api_key Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)
