import Config

config :my_metex_otp,
  open_weather_map_api_key: :none,

  # In practice, you will have to pass the mock to the system under the test.
  # Since the system under test relies on application configuration,
  # set it before the tests starts in order to utilize the async: property.
  open_weather_map_adapter: MyMetexOtp.OpenWeatherMapMock

# example use: "value1" = Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)
# example use: @api_key Application.fetch_env!(:my_metex_otp, :open_weather_map_api_key)
