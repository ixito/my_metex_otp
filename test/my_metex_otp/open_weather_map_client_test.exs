defmodule MyMetexOtp.OpenWeatherMapClientTest do
  use ExUnit.Case, async: true

  import Mox

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "invoke get/1" do
    MyMetexOtp.OpenWeatherMapMock
    |> expect(:get, fn "good_url" -> {:ok, %HTTPoison.Response{}} end)
    |> expect(:get, fn "bad_url" -> {:error, %HTTPoison.Error{}} end)

    MyMetexOtp.OpenWeatherMapMock.get("good_url")
    MyMetexOtp.OpenWeatherMapMock.get("bad_url")
  end
end
